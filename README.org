#+title: Dmenu Scripts

* About
This is a collection of useful shell scripts that I have written, and are meant to be used with dmenu. A lot of these scripts are written with the help of [[https://www.youtube.com/@DistroTube][Distrotube's]] [[https://gitlab.com/dwt1/dmscripts/-/tree/master/][dmscripts]] repo.

This repository includes the following scripts:
- ~find-org-stories~: A script which uses find to list .org files in a directory. This was written as my first ever shell script, used to quickly open the stories I occasionally write in emacs.
- ~logout-script~: A simple script for locking the screen, suspending, logging out, rebooting or shutting the system down. Uses systemctl and slock.
- ~proc-killer~: A script which uses ps to list currently running processes, and kills the chosen one.

* Dependencies
Obviously, every script requires ~dmenu~. For each individual script's dependencies, check the ~# Dependencies: ...~ line in each script.
** Important Note
These scripts have been built with and use flags that are part of my *custom build of dmenu* found in my [[https://www.gitlab.com/VaronKING/Dotfiles][Dotfiles repo]]. Especially the *emojis* used throughout a lot of these scripts require the *Emoji Highlight* dmenu patch. The emojis themselves use *Font Awesome Free 6*. For clarity's sake, these dependencies are not mentioned in each script.

* Installation
Simply ~git clone~ this repo. For ease of use, I've added the path to this directory to my shell's PATH.

* Found a problem?
Make sure to report any problems you come across by opening an issue on this repo. Any known future problems or incompatibilities will be listed in this README.

* License
This repository is licensed under the GNU GPLv3.
