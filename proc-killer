#!/usr/bin/env bash

# This is a process killer script using ps and dmenu.
# Dependencies: dmenu
# https://www.gitlab.com/VaronKING/

DMENU="dmenu -i -F -z 1888 -x 16 -h 25 -l 15 -p"

# We filter out the first line and unnecessary programs (ps, awk, etc) using grep.
choice=$(ps --user "$(whoami)" -F \
    | awk '{print $2, $11}' \
    | sort -V -k 2,2\
    | grep -ve "PID" -e "ps" -e "awk" -e "grep" -e "sort" -e "bash" -e "dmenu" \
    | ${DMENU} " Choose a program to kill: " -g 2)

if [[ -n ${choice} ]]; then
    # We grab the program name(s) using awk on the ${choice} variable. Line breaks are replaced with spaces, and the trailing space is removed with xargs.
    # We pipe No/sigterm/sigkill into dmenu, as a confirmation.
    progName=$(echo "${choice}" | awk '{print $2}' | tr '\n' ' ' | xargs)
    echo "Kill ${progName}?"
    answer=$(printf ":r  No\n:b  sigterm\n:y  sigkill" | ${DMENU} " Kill selected program(s)? (${progName})")

    if [[ ${answer} = ":r  No" ]]; then
        echo "Program quit." && exit 0

    elif [[ ${answer} = ":b  sigterm" || ${answer} = ":y  sigkill" ]]; then
	# We can use either sigterm or sigkill depending on what the user chose.
	if [[ ${answer} = ":b  sigterm" ]]; then
	    sig=15

	else sig=9
	fi

	# A for loop is used in case we want to kill more than 1 program at a time.
	# In this case, the loop repeats from 1 up to the line number of ${choice} (the number of programs selected).
	for i in $(seq 1 "$(echo "${choice}" | wc -l)"); do
	    # An awk variable is used to represent the index. This way we will always print out the current program's name.
	    currentProg=$(echo "${progName}" | awk -v ind="$i" '{print $ind}')

	    # We get the program ID(S) by using awk to print out only the ID(S), and then sed to grab the appropriate line depending on the current index of the loop.
	    echo "Terminating ${currentProg} ($(echo "${answer}" | awk '{print $NF}'))."
	    kill -s $sig "$(echo "${choice}" | awk '{print $1}' | sed ''"$i"'q;d')"
	    echo "${currentProg} terminated."

	done

    else echo "Program quit." && exit 0
    fi

else echo "Program quit." && exit 0
fi
